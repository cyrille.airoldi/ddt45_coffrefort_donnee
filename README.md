# Projet de coffre-fort de la donnée pour la DDT du Loiret

Sécurisations et partages des différents traitements de données via R utilisés dans l'administration de données géographiques

- 0 : Répertoire ADMINISTRATION_DONNEES : Différents scripts de gestion des fichiers sur les serveurs internes de la DDT 45
- 1 : Geoide : Différents scripts de traitement des données geoide distribution
- 2 : ign.R : Traitements des données géographiques de l'ign en libre service ()hors protocole national de diffusion)
- 3 : insee.R : Traitements des données fournies par l'INSEE
- 4 : opendata.R : Traitements des données opendata de différents producteurs

L'ensemble des traitements s'appuient sur l'organisation geoide des répertoires et fichiers selon les prescriptions du ministère.
