#------------------------------------------------------------------------------
# Version 0.1 en développement
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# Chargement des librairies
#------------------------------------------------------------------------------

library(tidyverse)
library(sf)
library(readxl)
library(openxlsx)
library(httr)
library(jsonlite)

#------------------------------------------------------------------------------
# Déclaration des variables
#------------------------------------------------------------------------------

## url des jeux de données

# url des zonages d'études
url_aire_urbaine="https://www.insee.fr/fr/statistiques/fichier/2115011/AU2010_au_01-01-2020.zip"
url_unite_urbaine="https://www.insee.fr/fr/statistiques/fichier/2115018/UU2010_au_01-01-2020.zip"  
url_zone_emploi="https://www.insee.fr/fr/statistiques/fichier/4652957/ZE2020-au-01-01-2020.zip"
url_bassin_vie="https://www.insee.fr/fr/statistiques/fichier/2115016/BV2012_au_01-01-2020.zip"
url_densite_communale="https://www.insee.fr/fr/statistiques/fichier/2114627/grille_densite_2019.zip"

# url de la base permanente des équipements
url_bpe="https://www.insee.fr/fr/statistiques/fichier/3568638/bpe19_ensemble_xy_csv.zip"
url_bpe_gamme="https://www.insee.fr/fr/statistiques/fichier/3568650/BPE_gammes_2019_internet.xlsx"

## Déclaration des variables géographiques
dept="45"
regio="24"

# Chemins de la bd_topo
chem_dep_bdtopo="R:/BDTOPO_V3/ADMINISTRATIF/N_DEPARTEMENT_TOPO_045.shp"
chem_communes_bdtopo="R:/BDTOPO_V3/ADMINISTRATIF/N_COMMUNE_TOPO_045.shp"

## Nomanclatures  
# BPE Crise
nomanclature_bpe_crise=c("A101","A104","A502",
                         "B101","B102","B201","B202","B316",
                         "C101","C102","C104","C105",
                         "C201","C301","C302","C303",
                         "C601","C602","C603","C604","C605","C609",
                         "D106","D107","D108","D113","D601",
                         "D301","D401","D603",
                         "F116","F121")

## Chemins des résultats
chem_unite_urbaine="M:/_AMENAGEMENT URBANISME/N_ZONAGES_ETUDE/N_URBAINE_UNITE_ZSUP_045/1-Données entrantes"
chem_result_bpe="M:/_CULTURE SOCIETE SERVICES/2-traitement"

#------------------------------------------------------------------------------
# zonages d'étude insee
#------------------------------------------------------------------------------

# Chargement et decompression des fichiers
# Création de la couche des Unités urbaines

#------------------------------------------------------------------------------
# Recensement général de la population
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# bases locales insee
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# base permanente des equipements
#------------------------------------------------------------------------------

# Chargement du fichier zip géolocalisé
# Création des répertoires temporaires
temp <- tempfile()
temp2 <- tempfile()

# Chargement du zip  
download.file(url_bpe,temp)

# unzip
unzip(zipfile = temp, exdir = temp2)

# liste des fichiers dezippés
bpe_liste_fichiers<-list.files(temp2, full.names=TRUE)

chem_bpe<-bpe_liste_fichiers[[1]]
chem_bpe_nomenclature<-bpe_liste_fichiers[[2]]

# Chargement de la BPE
bpe_donnees<-read.csv(chem_bpe,sep=";",fileEncoding = "UTF-8",stringsAsFactors = F)%>%
  filter(DEP==dept & LAMBERT_X!="")

# Préparation de la nomenclature BPE
bpe_nomenclature<-read.csv(chem_bpe_nomenclature,sep=";",fileEncoding = "UTF-8",stringsAsFactors = F)  %>%
  filter(COD_VAR=="TYPEQU")%>%
  select(COD_MOD,LIB_MOD)%>%
  rename(TYPEQU=COD_MOD)

# Concaténation de la BPE
bpe_donnees<-left_join(bpe_donnees,bpe_nomenclature,by="TYPEQU")

# Géolocalisation de la BPE
bpe_sf<-st_as_sf(bpe_donnees, coords = c("LAMBERT_X", "LAMBERT_Y"),crs=2154)

# Chargements des gammes d'équipement
bpe_gamme<-read.xlsx(url_bpe_gamme,startRow = 4)

# Formatage des gammes d'équipement pour intégration à table géographique bpe
bpe_gamme<-melt(bpe_gamme,id=c("code.équipement","libellé.équipement","gamme","domaine_code","domaine_libellé"))

bpe_gamme<- bpe_gamme  %>%
  rename(RTYPEQU="code.équipement",RTYP_LIB="libellé.équipement",GAMME="gamme",DOM_LIB="domaine_libellé",TYPEQU="value")  %>%
  select(RTYPEQU,RTYP_LIB,GAMME,DOM_LIB,TYPEQU)

bpe_gamme<-bpe_gamme%>%
  filter(!is.na(TYPEQU))

# Intégration des gammes d'équipements à la table géographique
bpe_sf<-left_join(bpe_sf,bpe_gamme,by="TYPEQU")

bpe_sf$RTYPEQU[is.na(bpe_sf$RTYPEQU)] <- bpe_sf$TYPEQU[is.na(bpe_sf$RTYPEQU)]
bpe_sf$RTYP_LIB[is.na(bpe_sf$RTYP_LIB)] <- bpe_sf$LIB_MOD[is.na(bpe_sf$RTYP_LIB)]
bpe_sf$GAMME[is.na(bpe_sf$GAMME)] <- "Non classé"
bpe_sf$DOM_LIB[is.na(bpe_sf$DOM_LIB)] <- "Non retenu dans les gammes INSEE"

## Traitement de la base permanente des équipements version gestion de crise

# Selection de la nomanclature de crise
bpe_sf_crise<-bpe_sf%>%
  mutate(ORIGIN="BPE",INOND="N",TECH="N",NUCL="N")%>%
  filter(TYPEQU %in% nomanclature_bpe_crise)

#Chargement de la couche communale BDTOPO
communes<-st_read(chem_communes_bdtopo)

#Chargement de la couche service de la bdtopo
bdtopo_service<-st_read(chem_services_bdtopo)%>%
  st_transform(2154)%>%
  st_join(communes,join=st_intersects)%>%
  filter(NATURE=="Mairie")%>%
  rename(LIB_MOD=NATURE)%>%
  mutate(TYPEQU="TOP1",
         REG=regio,
         DEP=dept,
         DEPCOM=INSEE_COM,
         DCIRIS="",
         AN=year(DATE_TOP),QUALITE_XY=STATUT_TOP,RTYPEQU="Mairie",RTYP_LIB="",GAMME="Non classé",DOM_LIB="Donnée BDTOPO",ORIGIN="BDTOPO",INOND="N",TECH="N",NUCL="N")%>%
  select(TYPEQU,REG,DEP,DEPCOM,DCIRIS,AN,QUALITE_XY,LIB_MOD,RTYPEQU,RTYP_LIB,GAMME,ORIGIN,DOM_LIB,INOND,TECH,NUCL)

# Concaténation des résultats
bpe_bdtopo<-rbind(bpe_sf_crise,bdtopo_service)%>%
  mutate(AN=as.character(AN))

## Integration des zones de risque (en cours de développement - traitement manuel)  

# ouverture des couches de zones risques
zrppri<-st_read(zr_ppri)%>%
  st_transform(2154)
zrpprt<-st_read(zr_pprt)%>%
  st_transform(2154)
zrnuc <-st_read(zr_nucleaire)%>%
  st_transform(2154)

# MAJ de la colonne INOND
bpe_bdtopo<-st_join(bpe_bdtopo,zrppri ,join=st_within)  %>%
  select(1:16,IdAss)%>%
  mutate(INOND=replace(INOND,!is.na(IdAss),"O"))%>%
  select(1:16)

# MAJ de la colonne TECH
bpe_bdtopo<-st_join(bpe_bdtopo,zrpprt ,join=st_within)  %>%
  select(1:16,IdGen)%>%
  mutate(TECH=replace(TECH,!is.na(IdGen),"O"))%>%
  select(1:16)

# MAJ de la colonne NUCL
bpe_bdtopo<-st_join(bpe_bdtopo,zrnuc,join=st_within) %>%
  select(1:16,Nom)%>%
  mutate(NUCL=replace(NUCL,!is.na(Nom),"O"))%>%
  select(1:16)

## Création des couches géographiques
setwd(chem_result_bpe)

nom_bpe=paste0("N_BPE_P_0",dept,".shp")
write_sf(bpe_sf,nom_bpe,layer_options = "ENCODING=UTF-8")
nom_bpe_bdtopo=paste0("N_BPE_CRISE_P_0",dept,".shp")
write_sf(bpe_bdtopo,nom_bpe_bdtopo,layer_options = "ENCODING=UTF-8")

## Nettoyage
rm(url_bpe,
   url_bpe_gamme,
   temp,
   temp2,
   bpe_liste_fichiers,
   chem_bpe,
   chem_bpe_nomenclature,
   bpe_donnees,
   bpe_nomenclature,
   bpe_gamme,
   nom_bpe,
   bpe_sf,
   chem_result_bpe)

#------------------------------------------------------------------------------
# test API INSEE
#------------------------------------------------------------------------------

# appel au jeton insee d'accès aux api
source("C:/Users/cyrille.airoldi/Documents/Scripts_R/jeton_insee.R")

liste_code <- data.frame(codgeo = c("200023372","74056","74143","74266","74290"), nivgeo=c("EPCI","COM","COM","COM","COM"))


croisement <- "NA5_B-ENTR_INDIVIDUELLE"
jeu_donnees <- "GEO2017REE2017"
modalite <- "all.all"

setwd("D:/Scripts_R")


## Création de la fonction API INSEE

acces_api <- function(jeton, jeu_donnees, croisement, modalite, nivgeo, codgeo){
  
  ##  Connexion à l'API données locales pour récupérer les données ---------------------------
  # création de l'entête contenant le jeton préalablement obtenu sur le catalogue des API
  auth_header <- httr::add_headers('Authorization'= paste('Bearer',jeton)) # permet de se connecter avec le jeton
  
  modalite <- stringr::str_replace_all(modalite, '\\+', '%2B')
  
  # appel du webservice
  res <- httr::content(GET(paste0("https://api.insee.fr/donnees-locales/V0.1/donnees/geo-",
                                  croisement, "@", jeu_donnees, "/", nivgeo, "-", codgeo, ".", modalite),
                           auth_header), 
                       as="text", content_type_json(), encodin='UTF-8')
  
  ## Traitement des messages d'erreur -------------------------------------------------------
  # si le jeton est invalide
  if (stringr::str_detect(res, "Invalid Credentials. Make sure you have given the correct access token")){
    print('Erreur - Jeton invalide')
  } 
  # si les paramètres ne sont pas correcte
  else if (stringr::str_detect(res, "Aucune cellule ne correspond à la requête")){
    print('Erreur - Paramètre(s) de la requête incorrect(s)')
  }
  else if (stringr::str_detect(res, "Resource forbidden ")){
    print("Erreur - Scouscription à l'API données locales non réalisée")
  }
  else if (stringr::str_detect(res, "quota")==T){
    print("Erreur- Trop de requêtes, faire une pause")
  }
  
  else{
    
    # mise en forme du résultat
    res <- jsonlite::fromJSON(res)
    
    if (length(as.data.frame(res$Cellule)) == 0 ){
      print('Erreur - Paramètre(s) de la requête incorrect(s)')
    }
    else{
      
      ##  Mise en forme des données --------------------------------------------------------------
      
      # création d'une variable contenant le nombre de variable
      nb_var <- stringr::str_count(croisement, "-") + 1
      
      ##  construction de l'élément contenant les informations sur la zone détude ---------------
      # utilisation de l'information contenue dans la partie zone de la sortie 
      zone <- res$Zone
      info_zone <- as.data.frame(cbind(zone$'@codgeo',zone$'@nivgeo',do.call("cbind", zone$Millesime)))
      colnames(info_zone) <- c("codgeo","libgeo","millesime_geo","libelle_sans_article","code_article")
      
      ##  construction de l'élément contenant les informations sur la source ---------------------
      # utilisation de l'information contenue dans la partie croisement de la sortie 
      croisement <- res$Croisement
      source <- as.data.frame(do.call("cbind", croisement$JeuDonnees))
      colnames(source) <- c("jeu_donnees", "millesime_donnees", "lib_jeu_donnees","lib_source")
      
      # ajout d'une nouvelle variable contenant la référence à la source à diffuser en même temps que les données
      # du type Insee, Demographie des entreprises et des établissements 2017, géographie au 01/012017
      source <- cbind(source, info_zone$millesime_geo) %>%
        dplyr::rename(millesime_geo = "info_zone$millesime_geo") %>%
        dplyr::mutate(source = paste0("Insee, ", lib_source, " ", millesime_donnees, ", géographie au 01/01", millesime_geo))
      
      ##  construction de l'élément contenant les informations sur les variables ------------
      # utilisation de l'information contenur dans la partie variable de la sortie ###
      variable <- res$Variable
      # temp <- variable$Modalite
      info_modalite <- as.data.frame(cbind(variable$'@code',variable$Libelle))
      
      # le programme diffère en fonction du nombre de variables dans le croisement
      # les modalités des variables sont stockées dans des listes. On va donc associer les modalités aux variables
      liste_code <- NULL
      if (nb_var > 1) {
        for (i in 1:length(variable$Modalite)) {
          if (! is.data.frame(variable$Modalite[[i]])){
            variable$Modalite[[i]] <- as.data.frame(variable$Modalite[[i]],stringsAsFactors=FALSE) %>%
              rename ('@code'=X.code, '@variable'=X.variable)}
          
          liste_code <- variable$Modalite[[i]] %>% 
            dplyr::mutate(V1 = info_modalite[i,]$V1) %>% 
            dplyr::mutate(V2 = info_modalite[i,]$V2) %>% 
            dplyr::bind_rows(liste_code) %>% 
            dplyr::select(V1, V2, '@code', '@variable', Libelle)
        }
      } else {
        liste_code <- cbind(info_modalite,variable$Modalite)
      }
      liste_code <- liste_code %>% 
        dplyr::select(-one_of('@variable'))
      colnames(liste_code) <- c("variable","lib_varible","modalite","lib_modalite")
      
      ##  construction de l'élément contenant les valeurs  --------------------------
      # création table donnees
      cellule<-as.data.frame(res$Cellule)
      var <- cellule$Modalite
      
      # les modalités*variables sont sotckés dans des listes.
      var_tot <- NULL
      if (nb_var > 1) {
        for (i in (1:length(var))){
          var_tot <- rbind(var_tot,cbind(t(var[[i]][1]),i)) # ajout d'une variable numéro de ligne pour s'assurer que l'ordre est ok
        }
        colnames(var_tot) <- c(t(var[[1]][2]),"nb_ligne")
        
        donnees <- cbind(cellule$Zone, cellule$Mesure,var_tot, cellule$Valeur) %>%
          dplyr::select(-one_of("nb_ligne"))
        colnames(donnees) <- c("codgeo","nivgeo","mesure","lib_mesure",c(t(var[[1]][2])), "valeur")
        
      } else {
        donnees <- do.call("cbind",cellule) %>%
          dplyr::select(-one_of("Modalite.@variable"))
        colnames(donnees) <- c("codgeo","nivgeo","mesure","lib_mesure",var[[2]][2], "valeur")
      }
      
      donnees <- as.data.frame(donnees)
      
      return(list(donnees=donnees, liste_code=liste_code, info_zone=info_zone, source = source))
    }
  }
}


## test

# Le nombre de requête est limitée à 30 requêtes par minutes. 
# Ajout d'une ligne dans le code pour attendre 2 secondes entre chaque requête 
# afin de pouvoir lancer plus de 30 requêtes avec le même code
acces_api_temp <- function(jeton, jeu_donnees, croisement, modalite, nivgeo, codgeo){
  temp <- acces_api(jeton, jeu_donnees, croisement, modalite, nivgeo, codgeo)
  Sys.sleep(2)
  return(temp)
}

sortie <- mapply(acces_api_temp,
                 jeton, jeu_donnees, croisement, 
                 modalite, liste_code$nivgeo, liste_code$codgeo,USE.NAMES = TRUE)

# création de variables contenant les données, métas
# donnees : données statistique (cube)
# liste_code : modalités de chaques variables
# info_zone : information sur les zones demandées
# source : information sur la source et le jeu de données demandé

donnees <- NULL
info_zone <- NULL
for (i in 1:dim(sortie)[2]){
  donnees <- rbind(donnees,sortie[,i]$donnees)
  info_zone <- rbind(info_zone,sortie[,i]$info_zone)
}
liste_code <- sortie[,1]$liste_code # la liste de code est la même pour tous les codes géographiques
source <- sortie[,1]$source # la source est la même pour tous les codes géographiques

export <- list(donnees, info_zone, liste_code, source)
names(export) <- c('donnees', 'info_zone','liste_code','source')
# export dans un fichier xlsx qui se nomme type jeu_donnee_croisement tous les croisements exportés. 
# le fichier excel contient tous les codes géographiques
# un onglet par type de sortie

write.xlsx(export, 'test.xlsx')