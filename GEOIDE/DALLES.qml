<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis styleCategories="Symbology|Forms|Actions|Rendering" simplifyLocal="1" simplifyDrawingHints="1" simplifyDrawingTol="1" version="3.10.6-A Coruña" simplifyAlgorithm="0" minScale="1e+08" hasScaleBasedVisibilityFlag="0" simplifyMaxScale="1" maxScale="0">
  <renderer-v2 forceraster="0" type="singleSymbol" symbollevels="0" enableorderby="0">
    <symbols>
      <symbol type="fill" name="0" alpha="1" clip_to_extent="1" force_rhr="0">
        <layer pass="0" class="SimpleFill" locked="0" enabled="1">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="152,125,183,0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.26"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" name="name" value=""/>
              <Option name="properties"/>
              <Option type="QString" name="type" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <attributeactions>
    <defaultAction key="Canvas" value="{03b7d070-f80c-4e7c-9749-13e4eb989e93}"/>
    <actionsetting action="import os&#xd;&#xa;repertoire = os.path.dirname( unicode( qgis.utils.iface.activeLayer().dataProvider().dataSourceUri() ) )&#xd;&#xa;fic =repertoire+&quot;\DALLES\\&quot;+&quot;[% &quot;dalle&quot; %]&quot;&#xd;&#xa;layer =qgis.utils.iface.activeLayer()&#xd;&#xa;qgis.utils.iface.addRasterLayer(fic,&quot;[% &quot;dalle&quot; %]&quot;)&#xd;&#xa;qgis.utils.iface.setActiveLayer(layer)&#xd;&#xa;" id="{0f51be4d-a4d4-4502-9152-4f18fb3b8904}" shortTitle="" isEnabledOnlyWhenEditable="0" notificationMessage="" type="1" name="Ouvrir dalle" capture="1" icon="">
      <actionScope id="Canvas"/>
      <actionScope id="Feature"/>
      <actionScope id="Layer"/>
    </actionsetting>
  </attributeactions>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui sera appelée à l'ouverture du formulaire.

Utilisez cette fonction pour ajouter plus de fonctionnalités à vos formulaires.

Entrez le nom de la fonction dans le champ "Fonction d'initialisation Python".
Voici un exemple à suivre:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")

]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="dalle"/>
    <field editable="1" name="repertoire"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="dalle"/>
    <field labelOnTop="0" name="repertoire"/>
  </labelOnTop>
  <widgets/>
  <layerGeometryType>2</layerGeometryType>
</qgis>
