#------------------------------------------------------------------------------
# Version 1.0
#
# Editer automatiquement les liens surchargés pour des projets 
# d'atlas cartographiques sous geodie carto V2
# 
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# Chargement des librairies
#------------------------------------------------------------------------------
library(tidyverse) 
library(RPostgres) #Package pour se connecter à une base postgres
library(sf) #Package pour le traitement des objets geographiques (remplage rgeos,rgdal et sp)

#------------------------------------------------------------------------------
# Déclaration des variables
#------------------------------------------------------------------------------
# Referentiel servant pour l'atlas
chem_shp="R:/ADMINEXPRESS/COMMUNE.shp"

# Lien de la carte geoide carto 2
url_geoidecartov2="https://carto2.geo-ide.din.developpement-durable.gouv.fr/frontoffice/?map=9f3b6970-e4b2-4657-8246-df105ff37d70"

# Répertoire de résultat
chem_result="C:/Users/cyrille.airoldi/Desktop/test"

# Projection geoide carto (forcés à 3857)
proj_geoidecarto=3857

#------------------------------------------------------------------------------
# Traitement 
#------------------------------------------------------------------------------

# Chargement du shp
shp<-st_read(chem_shp)%>%
  st_transform(proj_geoidecarto) #Seul ce système de coordonnées fonctionne pour la surcharge en juillet 2021

# Creation des bbox
bbox_list <- lapply(st_geometry(shp), st_bbox)

# Création du dataframe
Extend <- data.frame(matrix(unlist(bbox_list), nrow=length(bbox_list), byrow=TRUE))

# Renommage des champs
names(Extend) <- names(bbox_list[[1]])

# Création du lien
Extend<-Extend%>%
  mutate(url=paste0(url_geoidecartov2,"&extent=",xmin,",",ymin,",",xmax,",",ymax))

setwd(chem_result)

write.table(Extend,"url_atlas_geoidecartov2.csv",sep=";", row.names = FALSE)